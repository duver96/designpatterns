package com.designpatterns.designpatterns.persistencia;

public class CreatorOracle extends ConexionFactory {
    @Override
    public ConexionDB conectar() {
        return new ConexionOracle();
    }
}
