package com.designpatterns.designpatterns.persistencia;

public abstract class ConexionFactory {

    public String crearConexion(){
        ConexionDB conexionDB = conectar();
        return conexionDB.conectar();
    }

    public String cerrarConexion(){
        ConexionDB conexionDB = conectar();
        return conexionDB.desconectar();
    }

    public abstract ConexionDB conectar();
}
