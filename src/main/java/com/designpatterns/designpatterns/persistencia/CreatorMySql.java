package com.designpatterns.designpatterns.persistencia;

public class CreatorMySql extends ConexionFactory {

    @Override
    public ConexionDB conectar() {
        return  ConexionMysql.getInstance();
    }

}
