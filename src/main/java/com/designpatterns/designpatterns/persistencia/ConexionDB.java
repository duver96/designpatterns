package com.designpatterns.designpatterns.persistencia;

public interface ConexionDB {



    String conectar();

    String desconectar();
}
