package com.designpatterns.designpatterns.persistencia;

public class ConexionMysql  implements ConexionDB {
    private static ConexionMysql mysql;

    private ConexionMysql() {
    }
    public static ConexionMysql getInstance(){
        if(mysql==null){
           mysql= new ConexionMysql();
            System.out.printf("Nueva instancia mysql");
        }
        return mysql;
    }

    @Override
    public String conectar() {

        return "conecto MySql";

    }

    @Override
    public String desconectar() {
        return "desconecto MySQL";
    }
}
