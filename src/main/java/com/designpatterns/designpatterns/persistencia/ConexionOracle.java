package com.designpatterns.designpatterns.persistencia;

public class ConexionOracle implements ConexionDB {

    @Override
    public String conectar() {
        return "conecto ORACLE";
    }

    @Override
    public String desconectar() {
        return "desconecto ORACLE";
    }
}
