package com.designpatterns.designpatterns.entrada.archivo;

public interface Entrada {

    String cargarData(String texto);
}
