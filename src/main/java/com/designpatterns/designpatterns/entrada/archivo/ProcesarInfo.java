package com.designpatterns.designpatterns.entrada.archivo;

public abstract class ProcesarInfo {

    public String procesar(String texto){
        Entrada procesarEntrada = processEntrada(texto);
        return procesarEntrada.cargarData(texto);
    }
    public abstract Entrada processEntrada(String texto);
}
