package com.designpatterns.designpatterns.entrada.archivo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CargaArchivo implements Entrada{


    @Override
    public String cargarData(String texto) {
        return texto + "process archivo";

    }
}
