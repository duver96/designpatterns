package com.designpatterns.designpatterns.entrada.archivo;

public class ArchivoProcesatData extends ProcesarInfo {

    public ArchivoProcesatData() {
        super();
    }


    @Override
    public Entrada processEntrada(String texto) {
        return new CargaArchivo();
    }
}
